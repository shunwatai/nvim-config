# Install language servers

1. Use `yarn` or `npm` to install the following packages

yarn global add typescript typescript-language-server vscode-langservers-extracted pyright

2. Then add the node bin path in `bashrc` or `zshrc`
export PATH=$(yarn global bin):$PATH

3. run the script to create the symlink to `~/.config/nvim/`

sh ./install.sh

4. Install the support languages for treesitter in vim

:TSInstall html css json json5 jsdoc yaml python go dot dockerfile javascript tsx typescript bash vim regex ql pug php make vue rst erlang svelte c cpp http
