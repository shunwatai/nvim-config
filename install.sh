#!/bin/bash
#set -e

WORK_DIR=$(pwd)
mkdir -p ~/.vim/{tmp,undodir}

# create nvim & redshift config directory 
mkdir -p /home/$(whoami)/.config/nvim/ /home/$(whoami)/.config/redshift/ /home/$(whoami)/.config/lazygit/ /home/$(whoami)/.config/kitty/

# get Fantasque nerdfont 
mkdir -p ~/.fonts && cd $_ && curl --silent -LOJ https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/FantasqueSansMono/Regular/complete/Fantasque%20Sans%20Mono%20Regular%20Nerd%20Font%20Complete.ttf
# get Victor nerdfont 
curl --silent -LOJ https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/VictorMono/Semi-Bold/complete/Victor%20Mono%20SemiBold%20Nerd%20Font%20Complete%20Mono.ttf
curl --silent -LOJ https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/VictorMono/Semi-Bold-Italic/complete/Victor%20Mono%20SemiBold%20Italic%20Nerd%20Font%20Complete%20Mono.ttf
# get adobe source code pro nerdfont 
curl --silent -LOJ https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/SourceCodePro/Semibold/complete/Sauce%20Code%20Pro%20Semibold%20Nerd%20Font%20Complete%20Mono.ttf
curl --silent -LOJ https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/SourceCodePro/Semibold-Italic/complete/Sauce%20Code%20Pro%20Semibold%20Italic%20Nerd%20Font%20Complete%20Mono.ttf
# get terminus nerdfont 
curl --silent -LOJ https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/Terminus/terminus-ttf-4.40.1/Regular/complete/Terminess%20\(TTF\)%20Nerd%20Font%20Complete%20Mono.ttf
curl --silent -LOJ https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/Terminus/terminus-ttf-4.40.1/Italic/complete/Terminess%20\(TTF\)%20Italic%20Nerd%20Font%20Complete%20Mono.ttf
fc-cache -f

# get xfce4-terminal theme
sudo curl https://raw.githubusercontent.com/folke/tokyonight.nvim/main/extras/xfceterm_tokyonight_night.theme -o /usr/share/xfce4/terminal/colorschemes/tokyonight-night.theme

# get Plug plugin manager
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
         https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'

# get tpm tmux package manager
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm || true

# craete symlink for the config files
cd $WORK_DIR
ln -sf $(pwd)/init.vim ~/.config/nvim/init.vim
ln -sf $(pwd)/plugin ~/.config/nvim/plugin
ln -sf $(pwd)/tmux.conf ~/.tmux.conf
ln -sf $(pwd)/zshrc ~/.zshrc
ln -sf $(pwd)/redshift.conf.sample ~/.config/redshift/redshift.conf
ln -sf $(pwd)/lazygit.yml ~/.config/lazygit/config.yml
ln -sf $(pwd)/kitty.conf.sample ~/.config/kitty/kitty.conf


echo 'done'
