lua << EOF
require'nvim-treesitter.configs'.setup {
  highlight = {
    enable = true,
    additional_vim_regex_highlighting = false,
  },
  indent = {
    enable = true,
  },
  ensure_installed = {
    'astro', 'css', 'glimmer', 'graphql', 'html', 'javascript',
    'lua', 'php', 'python', 'scss', 'svelte', 'tsx', 'twig',
    'typescript', 'vim', 'vue', 'c', 'java', 'python', 'json', 'yaml', 'go'
  },
}
require('ts_context_commentstring').setup{
  enable_autocmd = false,
}
EOF

