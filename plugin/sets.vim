set background=dark              " dark side is better
set tabstop=2 softtabstop=2      " look at the menu :h tabstop :h softtabstop
set shiftwidth=2
set expandtab
set smartindent
set guicursor=
set nu
set relativenumber
set nohlsearch
set hidden
set noerrorbells
set ignorecase
set smartcase
set backspace=2
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile
set hlsearch
set incsearch
set termguicolors
set scrolloff=8
set noshowmode
set completeopt=menuone,noinsert,noselect
set colorcolumn=80
set signcolumn=yes
set mouse+=a
set showmatch
set ruler
set cursorline
set wildmenu
set lazyredraw
set wrap linebreak nolist
set nofoldenable      " disable default folding
" set foldenable      " fold by default
" set foldlevelstart=1   " open most folds by default
set foldlevel=10
" set foldnestmax=2      " 10 nested fold max
set foldmethod=indent   " fold based on indent level
"set foldmethod=expr
set foldexpr=nvim_treesitter#foldexpr()
set noshowmode    " why need showing the mode when you using airline or light line?
set encoding=utf-8  " set encoding
set laststatus=2  " enable statusline for plugin lightline.vim
set clipboard^=unnamed,unnamedplus " make sure xclip installed.  yy then ctrl+v, ctrl+c then p
syntax on

set cmdheight=1
set updatetime=50
set shortmess+=c

set path+=src/** " include src/ when using gf for jump to file
