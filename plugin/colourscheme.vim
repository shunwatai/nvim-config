" gruvbox settings
"colorscheme gruvbox
"let g:tmuxline_theme = 'vim_statusline_3'
"
"func! s:transparent_background()
"  highlight Normal guibg=NONE ctermbg=NONE
"  highlight NonText guibg=NONE ctermbg=NONE
"  " Hihlighting
"  highlight Comment cterm=italic gui=italic
"  "highlight Todo cterm=bold,italic gui=bold,italic
"  "if, else etc
"  "highlight Conditional cterm=italic gui=italic
"  "highlight Keyword cterm=italic gui=italic
"  highlight Character cterm=italic gui=italic
"  "return, let, const etc
"  "highlight Statement cterm=italic,bold gui=italic,bold
"  "highlight Identifier cterm=italic,bold gui=italic,bold
"  "console, function args etc
"  highlight Special cterm=italic,bold gui=italic,bold
"endf
"autocmd ColorScheme * call s:transparent_background()


lua << EOF
---- Tokyonight config ----
require("tokyonight").setup({
  style = "moon", -- The theme comes in three styles, `storm`, a darker variant `night` and `day`
  transparent = true, 
  terminal_colors = true, 
  styles = {
    comments = { italic = true },
    keywords = { italic = true },
    functions = {},
    variables = {},
    -- Background styles. Can be "dark", "transparent" or "normal"
    sidebars = "dark", -- style for sidebars, see below
    floats = "transparent", -- style for floating windows
  },
  sidebars = { "terminal", "help" }, -- Set a darker background on sidebar-like windows. For example: `["qf", "vista_kind", "terminal", "packer"]`
  hide_inactive_statusline = false, -- Enabling this option, will hide inactive statuslines and replace them with a thin border instead. Should work with the standard **StatusLine** and **LuaLine**.
  dim_inactive = true, -- dims inactive windows
  lualine_bold = true, -- When `true`, section headers in the lualine theme will be bold

  --- You can override specific color groups to use other groups or a hex color
  --- fucntion will be called with a ColorScheme table
  ---@param colors ColorScheme
  on_colors = function(colors)
    colors.hint = colors.orange
    colors.error = "#ff0000"
  end,

  --- You can override specific highlights to use other groups or a hex color
  --- fucntion will be called with a Highlights and ColorScheme table
  ---@param highlights Highlights
  ---@param colors ColorScheme
  on_highlights = function(highlights, colors) end,
})

---- CatPpuccin config ----
require("catppuccin").setup({
  flavour = "mocha", -- latte, frappe, macchiato, mocha
  background = { -- :h background
      light = "latte",
      dark = "mocha",
  },
  transparent_background = true,
  show_end_of_buffer = false, -- show the '~' characters after the end of buffers
  term_colors = true,
  dim_inactive = {
      enabled = false,
      shade = "dark",
      percentage = 0.15,
  },
  no_italic = false, -- Force no italic
  no_bold = false, -- Force no bold
  styles = {
      comments = { "italic" },
      conditionals = { "italic" },
      loops = {},
      functions = { "italic" },
      keywords = {},
      strings = {},
      variables = {},
      numbers = {},
      booleans = {},
      properties = {},
      types = {},
      operators = {},
  },
  color_overrides = {
    all = {
      text = "#c6d0f5",
    }
  },
  custom_highlights = {},
  integrations = {
      cmp = true,
      gitsigns = true,
      nvimtree = true,
      telescope = true,
      notify = false,
      mini = false,
      -- For more plugins integrations please scroll down (https://github.com/catppuccin/nvim#integrations)
  },
})

-- Load the colorscheme
--vim.cmd[[colorscheme tokyonight-moon]]
vim.cmd[[colorscheme catppuccin-mocha]]
EOF
