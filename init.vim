" go back to last edit line
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif


call plug#begin('~/.vim/plugged')

Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
"Plug 'nvim-telescope/telescope-fzy-native.nvim'
Plug 'nvim-telescope/telescope-fzf-native.nvim', { 'do': 'make' }
Plug 'neovim/nvim-lspconfig'
Plug 'ray-x/lsp_signature.nvim'

Plug 'mfussenegger/nvim-jdtls'

" nvim-cmp plugins
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/nvim-cmp'

" For luasnip users.
Plug 'L3MON4D3/LuaSnip'
Plug 'saadparwaiz1/cmp_luasnip'
Plug 'rafamadriz/friendly-snippets'

Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'nvim-treesitter/nvim-treesitter-context'
Plug 'prettier/vim-prettier', { 'do': 'yarn install --frozen-lockfile --production' }
Plug 'sbdchd/neoformat'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'ryanoasis/vim-devicons'
Plug 'ray-x/go.nvim'

" for motion move
Plug 'ggandor/lightspeed.nvim'

Plug 'nvim-lualine/lualine.nvim'
"Plug 'itchyny/lightline.vim'
"Plug 'vim-airline/vim-airline'
"Plug 'vim-airline/vim-airline-themes'
"Plug 'ctrlpvim/ctrlp.vim'
"Plug 'Shougo/unite.vim'
Plug 'scrooloose/syntastic'
Plug 'tpope/vim-fugitive'
" Plug 'preservim/nerdtree'
Plug 'kyazdani42/nvim-tree.lua'
"Plug 'mg979/vim-visual-multi', {'branch': 'master'}
"Plug 'sheerun/vim-polyglot'
"Plug 'flazz/vim-colorschemes'
"Plug 'Valloric/YouCompleteMe'
Plug 'gruvbox-community/gruvbox'
Plug 'folke/tokyonight.nvim'
Plug 'catppuccin/nvim', {'as': 'catppuccin'}
Plug 'numToStr/Comment.nvim'
Plug 'JoosepAlviste/nvim-ts-context-commentstring'
Plug 'lewis6991/gitsigns.nvim'
Plug 'akinsho/toggleterm.nvim'
Plug 'romgrk/barbar.nvim'
Plug 'lukas-reineke/indent-blankline.nvim'
Plug 'karb94/neoscroll.nvim'
Plug 'mbbill/undotree'
Plug 'norcalli/nvim-colorizer.lua'
Plug 'petertriho/nvim-scrollbar'


" Initialize plugin system
call plug#end()

"mode lhs rhs
" Some key mapping "
let mapleader = " "

" Space will toggle folds!
nnoremap <space>f za

" switch tab by ctrl-L ctrl-H
nnoremap <C-L> gt
nnoremap <C-H> gT

" visual shifting (does not exit Visual mode)
vnoremap < <gv
vnoremap > >gv

" toggle paste mode for paste code
set pastetoggle=<F2>

" turn off search highlight
nnoremap <leader>n :nohlsearch<CR>

" For ctrlP "
"let g:ctrlp_map = '<c-p>'
"let g:ctrlp_cmd = 'CtrlP'
"set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.gz,*.bz2
"let g:ctrlp_show_hidden = 1
"let g:ctrlp_match_window = 'bottom,order:ttb'
"let g:ctrlp_switch_buffer = 0
"let g:ctrlp_working_path_mode = 0

" ctrl + n for nerdtree
" nmap <silent> <c-s> :NERDTreeToggle<cr>

" space-v to keep pasting without losting the register
nnoremap <leader>v "_dP 
" vim default file explorer
nnoremap <leader>e :Ex<CR>

nnoremap <leader>u :UndotreeShow<CR>

"https://www.youtube.com/watch?v=hSHATqh8svM
" behave vim
nnoremap Y y$

" show the matched keyword at the center of the screen
nnoremap n nzzzv
nnoremap N Nzzzv

" fine tune join line
nnoremap J mzJ`z

" undo break point
inoremap , ,<c-g>u
inoremap . .<c-g>u
inoremap ! !<c-g>u
inoremap ? ?<c-g>u

" jumplist mutations
nnoremap <expr> k (v:count > 5 ? "m'" . v:count : "") . 'k'
nnoremap <expr> j (v:count > 5 ? "m'" . v:count : "") . 'j'

" moving text
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv
inoremap <C-j> <esc>:m .+1<CR>==
inoremap <C-k> <esc>:m .-2<CR>==
nnoremap <leader>j :m .+1<CR>==
nnoremap <leader>k :m .-2<CR>==

" change a word by cn, then press . to change next matching
nnoremap cn *``cgn
nnoremap cN *``cgN

" remap esc to jj
inoremap jj <ESC>

" navigate quickfix list
nnoremap <C-j> :cnext<CR>zz
nnoremap <C-k> :cprev<CR>zz

" prettier
"let g:prettier#autoformat_require_pragma = 0
"let g:prettier#autoformat_config_present = 1
" Enable alignment
let g:neoformat_basic_format_align = 1
" Enable tab to spaces conversion
let g:neoformat_basic_format_retab = 1
" Enable trimmming of trailing whitespace
let g:neoformat_basic_format_trim = 1

let g:neoformat_try_node_exe = 1
" Enable auto format on save
"augroup fmt
"  autocmd!
"  autocmd BufWritePre * undojoin | Neoformat | wincmd e
"augroup END

" set lightline theme
" let g:lightline = {'colorscheme': 'tokyonight'}

" no more read-only permission issue
cnoremap w!! execute 'silent! write !sudo tee % >/dev/null' <bar> edit!
